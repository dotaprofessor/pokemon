using System;
using UnityEngine;

[Serializable]
public class PokeData : IComparable<PokeData>
{
    public int id;
    public string name;
    public string order;
    public string weight;
    public Sprites sprites;
    public Color bgColor;

    public int CompareTo(PokeData other)
    {
        return id.CompareTo(other.id);
    }
}

[Serializable]
public class Sprites
{
    public string back_default;
    public string back_shiny;
    public string front_default;
    public string front_shiny;
}