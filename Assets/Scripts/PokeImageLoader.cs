using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PokeImageLoader
{
    private static readonly Dictionary<int, Sprite> _spritesDict = new();

    public static IEnumerator GetSprite(int id, string uri, Action<Sprite> callback)
    {
        if (_spritesDict.ContainsKey(id) && _spritesDict[id] != null)
        {
            yield return null;
            callback(_spritesDict[id]);
        }
        else
        {
            using (var request = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.LogError(request.error);
                }
                else
                {
                    var spriteBytes = request.downloadHandler.data;
                    var pokemonTexture = new Texture2D(2, 2);
                    pokemonTexture.LoadImage(spriteBytes);
                    var spriteRect = new Rect(0, 0, pokemonTexture.width, pokemonTexture.height);
                    var sprite = Sprite.Create(
                        pokemonTexture,
                        spriteRect,
                        new Vector2(0.5f, 0.5f),
                        100.0f);
                    _spritesDict[id] = sprite;
                    callback(sprite);
                }
            }
        }
    }
}