using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PokeItem : MonoBehaviour
{
    public Image background;
    public TextMeshProUGUI name;
    public TextMeshProUGUI order;
    public TextMeshProUGUI weight;
    public Image icon;

    public void setData(PokeData pokeData)
    {
        name.text = string.Format("Name: {0}", pokeData.name);
        order.text = string.Format("Order: {0}", pokeData.order);
        weight.text = string.Format("Weight: {0}", pokeData.weight);
        background.color = pokeData.bgColor;
        Debug.Log(pokeData.id + " " + pokeData.name + " " + pokeData.sprites.front_default);
        StartCoroutine(PokeImageLoader.GetSprite(pokeData.id, pokeData.sprites.front_default,
            sprite => icon.sprite = sprite));
    }
}