using System.Collections;
using System.Collections.Generic;
using Mosframe;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokeController : MonoBehaviour
{
    public static List<PokeData> data2 = new();
    public DynamicScrollView scrollView;
    public Scrollbar scrollbar;

    private readonly Color[] colors =
    {
        Color.black,
        Color.red,
        Color.magenta,
        Color.blue,
        Color.gray
    };

    private int currentIndex = 1;
    private int requestRemain;

    private void Awake()
    {
        scrollbar.onValueChanged.AddListener(OnScrollBarValueChange);
    }

    private void Start()
    {
        StartCoroutine(GetPokemons(20));
    }

    public void OnScrollBarValueChange(float value)
    {
        if (value < 0 && requestRemain <= 0)
        {
            scrollView.scrollByItemIndex(Mathf.RoundToInt(currentIndex / 2));
            StartCoroutine(GetPokemons(10));
        }
    }

    private IEnumerator GetPokemons(int count)
    {
        requestRemain = count;
        for (var i = 0; i < count; i++) yield return requestPokemon(currentIndex++ + i);
    }

    private IEnumerator requestPokemon(int index)
    {
        var url = string.Format("https://pokeapi.co/api/v2/pokemon/{0}/", index);
        var request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.Success)
        {
            var pokemon = JsonUtility.FromJson<PokeData>(request.downloadHandler.text);
            pokemon.bgColor = colors[Random.Range(0, colors.Length - 1) % colors.Length];
            addPokemon(pokemon);
        }
        else
        {
            Debug.LogError($"Failed to retrieve Pokemon ID. Error: {request.error}");
        }

        requestRemain--;
    }

    private void addPokemon(PokeData pokeData)
    {
        var row = data2.Find(x => x.id == pokeData.id);
        if (row != null)
            return;
        data2.Add(pokeData);
        data2.Sort();
        scrollView.totalItemCount = Mathf.CeilToInt(data2.Count / 2);
    }
}