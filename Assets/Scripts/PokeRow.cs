using Mosframe;
using UnityEngine;

public class PokeRow : MonoBehaviour, IDynamicScrollViewItem
{
    public PokeItem[] pokeItems;

    public void onUpdateItem(int index)
    {
        if (index == -1) return;
        if (index >= PokeController.data2.Count) return;

        var left = PokeController.data2[index * 2];
        pokeItems[0].setData(left);
        var right = PokeController.data2[index * 2 + 1];
        if (right != null)
        {
            pokeItems[1].gameObject.SetActive(true);
            pokeItems[1].setData(right);
        }
        else
        {
            pokeItems[1].gameObject.SetActive(false);
        }
    }
}